@include('includes.header')

@include('includes.sidenav')

<main class="content">

    @include('includes.topnav')

    @yield('content')

    @include('includes.footer-content')

</main>

@include('includes.footer')